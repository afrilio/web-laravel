<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();//select * from pertanyaan
        //dd($pertanyaan);
        return view('pertanyaan.index', compact('pertanyaan'));//
    }

    public function create(){
        return view('pertanyaan.create');//
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
             "isi" => $request["isi"]
             
        ]);
        return redirect('/pertanyaan')-> with('success', 'Pertanyaan berhasil disimpan!');
    }

    public function show($id){
        $quest = DB::table('pertanyaan')->where('id', $id)->first();//untuk menampilkan 1 object saja
        //dd($quest);
        return view('pertanyaan.show', compact('quest'));
    }

    public function edit($id){
        $quest = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('quest'));
    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi']
                    ]);

        return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan!');
    }

    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus!');
    }
    
}
