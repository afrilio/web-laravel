<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/hello', function () {
    return 'hello ';
});

// Route::get('/register', function () {
//     return view('register');
// });

// Route::get('/home', function () {
//     return view('home');
// });

// Route::get('/welcome/{number}', function ($number) {
//     return view('welcom1', ["number" => $number]);
// });

//Route::get('/form', 'RegisterController@form');

Route::get('/home', 'HomeController@home');

Route::get('/register', 'AuthController@register');
Route::post('/register', 'AuthController@register_post');

Route::get('/welcom1', 'AuthController@welcom1');
Route::post('/welcom1', 'AuthController@welcom1_post');

Route::get('/master', function () {
    return view('master');
});

Route::get('/', function () {
    return view('items.index');
});

Route::get('/data-tables', function () {
    return view('items.data-tables');
});

Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');
