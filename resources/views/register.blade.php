<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <div>
        <form>
            <label for="fname">First name:</label><br><br>
            <input type="text"><br><br>
            <label for="lname">Last name:</label><br><br>
            <input type="text"><br><br>
            <label>Gender:</label><br><br>
            <input type="radio" name=gen value="0">Male<br>
            <input type="radio" name=gen value="1">Female<br>
            <input type="radio" name=gen value="2">Other<br><br>
            <label>Nationality:</label><br><br>
            <select>
                <option value="indo">Indonesian</option>
                <option value="singa">Singaporian</option>
                <option value="malay">Malaysian</option>
                <option value="aus">Australian</option>
            </select>
            <br><br>
            <label>Language Spoken:</label><br><br>
            <input type="checkbox" name="ch_bhs" value="0">Bahasa Indonesia<br>
            <input type="checkbox" name="ch_bhs" value="1">English<br>
            <input type="checkbox" name="ch_bhs" value="2">Other<br><br>
            <label>Bio:</label><br><br>
            <textarea cols="30" rows="10"></textarea><br>
            <input type="submit" value="Sign Up" formaction="/welcom1">
        </form>
    </div>
</body>
</html>