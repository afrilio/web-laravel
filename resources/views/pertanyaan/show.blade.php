@extends('adminlte.master')

@section('content')
  <div class="mt-2 ml-3">
    <h5> {{ $quest->judul }} </h5>
    <p> {{ $quest->isi }} </p>
  </div>
@endsection
