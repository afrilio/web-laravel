@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-2">
  @if (session('success'))
      <div class="alert alert-success">
        {{session('success')}}
      </div>
  @endif
  <a class="btn btn-primary" href="/pertanyaan/create" >Buat Pertanyaan Baru</a>
  <table class="table table-bordered">
  <thead>                  
    <tr>
      <th style="width: 10px">#</th>
      <th>Judul</th>
      <th>Isi</th>
      <th style="width: 40px">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($pertanyaan as $key => $quest)
    <tr>
      <td> {{ $key + 1 }} </td>
      <td> {{ $quest -> judul }} </td>
      <td> {{ $quest -> isi }} </td>
      <td style="display:flex">
        <a href="/pertanyaan/{{$quest->id}}" class="btn btn-info btn-sm">show</a>
        <a href="/pertanyaan/{{$quest->id}}/edit" class="btn btn-default btn-sm">edit</a>
        <form action="/pertanyaan/{{$quest->id}}" method="POST">
        @csrf
        @method('delete')
        <input type="submit" value="delete" class="btn btn-danger btn-sm">
        </form>
      </td>
    </tr>
  @empty
    <tr>
      <td colspan="4" align="center"> No Post </td>
    </tr>            
    @endforelse
  </tbody>
  </table>
</div>
@endsection